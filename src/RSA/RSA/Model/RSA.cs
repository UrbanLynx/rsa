﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace RSA.Model
{
    class RSA
    {
        public int publicKey;//публичный ключ
        private int privateKey;//приватный ключ
        public int n;
        
        public int Crypt(int m)//шифруем
        {
            return (int) BigInteger.ModPow(m, publicKey, n);
        }

        public int Decrypt(int c)//дешифруем
        {
            return (int)BigInteger.ModPow(c, privateKey, n);
        }

        private PrimeAlgo prime = new PrimeAlgo();
        public void Algorithm(int low, int high)
        {
            var p = prime.GeneratePrimeNumber(low, low + (high - low)/2);
            var q = prime.GeneratePrimeNumber(low + (high - low) / 2, high);
            n = p*q;
            var phi = (p - 1)*(q - 1);

            const int eLow = 2;
            const int eHigh = 30;
            publicKey = prime.GeneratePrimeNumber(eLow, eHigh);
            while (phi % publicKey !=0 )
            {
                publicKey = prime.GeneratePrimeNumber(eLow, eHigh);
            }

            privateKey = prime.InvMode(eHigh, phi);
        }

        public void Cipher(byte[] message)
        {
            var count = message.Length%4 == 0 ? message.Length/4 : message.Length/4 - 1;
            var cipher = new List<int>();
            for (int i = 0; i < count; i++)
            {
                cipher.Add(BitConverter.ToInt32(message, 4*i));
            }

        }
    }
}
