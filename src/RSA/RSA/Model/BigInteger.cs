﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RSA.Model
{
    public class BigInteger : IComparable<BigInteger>
    {
        private List<int> arr = new List<int>();
        private bool sign = true;
        private static int order = 1;
        private static int myBase = (int)Math.Pow(10, order);
        public override bool Equals(object obj)
        {
            BigInteger target = (BigInteger)obj;
            if (target.arr.Count == arr.Count)
            {
                for (int i = 0; i < arr.Count; i++)
                {
                    if (arr[i] != target.arr[i])
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
            return sign == target.sign;
        }
        public void ChangeSign(bool flag)
        {
            sign = flag;
        }
        public BigInteger(string s)
        {
            int tempValue = 0;
            int tempOrder = 0;
            sign = true;
            for (int i = s.Length - 1; i >= 0; i--)
            {
                if (i == 0 && s[0] == '-')
                {
                    sign = false;
                }
                else
                {

                    if (tempOrder == order)
                    {
                        arr.Add(tempValue);
                        tempValue = int.Parse(s[i].ToString());
                        tempOrder = 1;

                    }
                    else
                    {
                        tempValue = tempValue + (int)Math.Pow(10, tempOrder) * int.Parse(s[i].ToString());
                        tempOrder++;
                    }
                }

            }
            if (tempOrder != 0)
            {
                arr.Add(tempValue);
            }
        }
        private BigInteger(int[] arr, bool sign)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                this.arr.Add(arr[i]);
            }
            normalize(this.arr);
            this.sign = sign;
        }
        private BigInteger(List<int> arr, bool sign)
        {
            this.arr = normalize(arr);
            this.sign = sign;
        }

        public override string ToString()
        {
            StringBuilder ans = new StringBuilder();
            if (!sign)
            {
                ans.Append('-');
            }
            if (arr.Count == 0)
            {
                return "0";
            }
            ans.Append(arr[arr.Count - 1].ToString());
            for (int i = arr.Count - 2; i >= 0; i--)
            {
                string temp = arr[i].ToString();
                int zeroCount = order - temp.Length;
                for (int j = 0; j < zeroCount; j++)
                {
                    ans.Append('0');
                }
                ans.Append(temp);
            }
            return ans.ToString();
        }
        public BigInteger Add(BigInteger value)
        {
            if (this.sign && value.sign)
            {
                return this.AbsAdd(value);
            }
            if (!this.sign && value.sign)
            {
                return value.AbsSubstract(this);
            }
            if (this.sign && !value.sign)
            {
                return this.AbsSubstract(value);
            }
            BigInteger ans = this.AbsAdd(value);
            ans.ChangeSign(false);
            return ans;
        }
        public BigInteger Substract(BigInteger value)
        {
            if (this.sign && value.sign)
            {
                return this.AbsSubstract(value);
            }
            if (!this.sign && value.sign)
            {
                BigInteger ans = this.AbsAdd(value);
                ans.ChangeSign(false);
                return ans;
            }
            if (this.sign && !value.sign)
            {
                return this.AbsAdd(value);
            }
            return value.AbsSubstract(this);
        }
        private BigInteger AbsAdd(BigInteger value)
        {
            int k = 0;//значение переноса
            int n = Math.Max(arr.Count, value.arr.Count);//максимальная длина числа
            List<int> ans = new List<int>();//результирующий массив
            for (int i = 0; i < n; i++)
            {
                int tempA = (arr.Count > i) ? arr[i] : 0;//временное значение i-го разряда из первого числа
                int tempB = (value.arr.Count > i) ? value.arr[i] : 0;//временное значение i-го разряда из второго числа
                ans.Add(tempA + tempB + k);
                if (ans[i] >= myBase)//если результат получился больше базы
                {
                    ans[i] -= myBase;
                    k = 1;
                }
                else
                {
                    k = 0;
                }
            }
            if (k == 1)
            {
                ans.Add(k);
            }
            return new BigInteger(ans, true);
        }
        private BigInteger AbsSubstract(BigInteger value)
        {
            if (this.AbsCompareTo(value) == -1)
            {
                BigInteger temp = value.AbsSubstract(this);
                temp.ChangeSign(false);
                return temp;
            }
            int k = 0;//перенос
            int n = Math.Max(arr.Count, value.arr.Count);
            List<int> ans = new List<int>();
            for (int i = 0; i < n; i++)
            {
                int tempA = (arr.Count > i) ? arr[i] : 0;
                int tempB = (value.arr.Count > i) ? value.arr[i] : 0;
                ans.Add(tempA - tempB - k);
                if (ans[i] < 0)
                {
                    ans[i] += myBase;//прибавляем базу
                    k = 1;//задаем перенос
                }
                else
                {
                    k = 0;
                }
            }


            return new BigInteger(normalize(ans), true);
        }
        private static List<int> normalize(List<int> arr)
        {
            while (arr.Count > 1 && arr[arr.Count - 1] == 0)//удаляем лидирующие нули
            {
                arr.RemoveAt(arr.Count - 1);
            }
            return arr;
        }
        public BigInteger Multiply(int value)
        {
            BigInteger ans = this.AbsMultiply(Math.Abs(value));
            if (this.sign && value >= 0)
            {
                return ans;
            }
            if (!this.sign && value < 0)
            {
                return ans;
            }
            ans.ChangeSign(false);
            return ans;
        }
        public BigInteger AbsMultiply(int value)
        {
            if (value >= myBase)
            {
                throw new Exception("This value is bigger than base");
            }
            int k = 0;
            List<int> ans = new List<int>();
            for (int i = 0; i < arr.Count; i++)
            {
                long temp = (long)arr[i] * (long)value + k;
                ans.Add((int)(temp % myBase));
                k = (int)(temp / myBase);
            }
            ans.Add(k);
            return new BigInteger(normalize(ans), true);

        }

        public BigInteger Multiply(BigInteger value)
        {
            BigInteger ans = this.AbsMultiply(value);
            if ((this.sign && value.sign) || (!this.sign && !value.sign))
            {
                return ans;
            }
            ans.ChangeSign(false);
            return ans;
        }

        public BigInteger AbsMultiply(BigInteger value)
        {
            BigInteger ans = new BigInteger("0");
            for (int i = 0; i < arr.Count; i++)
            {
                BigInteger temp = value.AbsMultiply(arr[i]);
                for (int j = 0; j < i; j++)
                {
                    temp.arr.Insert(0, 0);
                }
                ans = ans.AbsAdd(temp);
            }
            return ans;
        }
        public BigInteger Divide(int v, out int r)
        {
            if (v == 0)
            {
                throw new Exception("divide by zero");
            }
            BigInteger ans = this.AbsDivide(Math.Abs(v), out r);
            if ((this.sign && v > 0) || (!this.sign && v < 0))
            {
                return ans;
            }
            ans.ChangeSign(false);
            r = -r;
            return ans;
        }
        private BigInteger AbsDivide(int v, out int r)
        {
            int[] ans = new int[arr.Count];//результат деления
            r = 0;//остаток
            int j = arr.Count - 1;
            while (j >= 0)
            {
                long cur = (long)(r) * (long)(myBase) + arr[j];
                ans[j] = (int)(cur / v);
                r = (int)(cur % v);
                j--;
            }

            return new BigInteger(ans, true);
        }
        private static int Divide(out BigInteger q, out BigInteger r, BigInteger u, BigInteger v)
        {
            BigInteger first = new BigInteger(u.ToString());
            first.ChangeSign(true);
            BigInteger second = new BigInteger(v.ToString());
            second.ChangeSign(true);
            int ans = AbsDivide(out q, out r, first, second);
            if ((u.sign && v.sign) || (!u.sign && !v.sign))
            {
                return ans;
            }
            q.ChangeSign(!q.sign);
            r.ChangeSign(!r.sign);
            return ans;

        }
        private static int AbsDivide(out BigInteger q, out BigInteger r, BigInteger u, BigInteger v)
        {
            //начальная инициализация
            int n = v.arr.Count;
            int m = u.arr.Count - v.arr.Count;
            int[] tempArray = new int[m + 1];
            tempArray[m] = 1;
            q = new BigInteger(tempArray, true);
            if (n == 1)
            {
                int tempr = 0;
                q = u.Divide(v.arr[0], out tempr);
                r = new BigInteger(tempr.ToString());
                return 0;
            }
            //Нормализация
            int d = (myBase / (v.arr[n - 1] + 1));
            u = u.Multiply(d);
            v = v.Multiply(d);
            if (u.arr.Count == n + m)
            {
                u.arr.Add(0);
            }
            //Начальная установка j
            int j = m;
            //Цикл по j
            while (j >= 0)
            {
                //Вычислить временное q 
                long cur = (long)(u.arr[j + n]) * (long)(myBase) + u.arr[j + n - 1];
                int tempq = (int)(cur / v.arr[n - 1]);//нормализация помогает не выпрыгнуть за границу типа
                int tempr = (int)(cur % v.arr[n - 1]);
                do
                {
                    if (tempq == myBase || (long)tempq * (long)v.arr[n - 2] > (long)myBase * (long)tempr + u.arr[j + n - 2])
                    {
                        tempq--;
                        tempr += v.arr[n - 1];
                    }
                    else
                    {
                        break;
                    }
                }
                while (tempr < myBase);
                //Умножить и вычесть
                BigInteger u2 = new BigInteger(u.arr.GetRange(j, n + 1), true);
                u2 = u2.Substract(v.Multiply(tempq));
                bool flag = false;
                if (!u2.sign)//если отрицательные
                {
                    flag = true;
                    List<int> bn = new List<int>();
                    for (int i = 0; i <= n; i++)
                    {
                        bn.Add(0);
                    }
                    bn.Add(1);
                    u2.ChangeSign(true);
                    u2 = new BigInteger(bn, true).Substract(u2);
                }

                //Проверка остатка
                q.arr[j] = tempq;
                if (flag)
                {
                    //Компенсировать сложение
                    q.arr[j]--;
                    u2 = u2.Add(v);
                    if (u2.arr.Count > n + j)
                        u2.arr.RemoveAt(n + j);

                }
                for (int h = j; h < j + n; h++)
                {
                    if (h - j >= u2.arr.Count)
                    {
                        u.arr[h] = 0;
                    }
                    else
                    {
                        u.arr[h] = u2.arr[h - j];
                    }
                }
                j--;
            }
            q.arr = normalize(q.arr);
            //Денормализация
            int unusedR = 0;

            r = new BigInteger(u.arr.GetRange(0, n), true).Divide(d, out unusedR);
            return 0;
        }

        public int CompareTo(BigInteger other)
        {
            if (this.sign && other.sign)//оба положительных
            {
                return AbsCompareTo(other);
            }
            if (this.sign && !other.sign)//первое число положительное
            {
                return 1;
            }
            if (!this.sign && other.sign)//второе число положительное
            {
                return -1;
            }
            return -1 * AbsCompareTo(other);//оба отрицательные
        }

        private int AbsCompareTo(BigInteger other)
        {
            int ans = 0;
            if (this.arr.Count == other.arr.Count)
            {
                for (int i = 0; i < this.arr.Count; i++)
                {
                    if (this.arr[i] > other.arr[i])
                    {
                        ans = 1;
                    }

                    if (this.arr[i] < other.arr[i])
                    {
                        ans = -1;
                    }

                }
            }
            else
            {
                if (this.arr.Count > other.arr.Count)
                {
                    ans = 1;
                }
                if (this.arr.Count < other.arr.Count)
                {
                    ans = -1;
                }
            }
            return ans;
        }
        public BigInteger Mod(BigInteger v)
        {
            BigInteger q;
            BigInteger r;
            if (this.AbsCompareTo(v) > -1)
            {
                if (v.arr.Count == 1)
                {
                    int tempr = 0;
                    this.Divide(v.arr[0], out tempr);
                    return new BigInteger(tempr.ToString());
                }
                Divide(out q, out r, this, v);
                return r;
            }
            else
            {
                return this;
            }
        }

        public BigInteger Div(BigInteger v)
        {
            BigInteger q;
            BigInteger r;
            if (this.AbsCompareTo(v) > -1)
            {
                if (v.arr.Count == 1)
                {
                    int tempr = 0;
                    return this.Divide(v.arr[0], out tempr);
                }
                Divide(out q, out r, this, v);
            }
            else
            {
                return BigInteger.Zero;
            }
            return q;
        }
        public BigInteger Pow(BigInteger k, BigInteger n)
        {
            BigInteger a = new BigInteger(arr, sign);
            BigInteger b = One;
            while (k.CompareTo(Zero) > 0)
            {

                int r = 0;
                BigInteger q = k.AbsDivide(2, out r);
                if (r == 0)
                {
                    k = q;
                    a = a.Multiply(a).Mod(n);// [ a = (a*a)%n; ]
                }
                else
                {
                    k = k.Substract(One);
                    b = b.Multiply(a).Mod(n);// [ b = (b*a)%n; ]
                }
            }
            return b;
        }
        public static BigInteger One = new BigInteger("1");
        public static BigInteger Zero = new BigInteger("0");
        public BigInteger Inverse(BigInteger n)
        {
            BigInteger a = new BigInteger(this.ToString());
            BigInteger b = n, x = Zero, d = One;
            while (a.CompareTo(Zero) == 1)//a>0
            {
                BigInteger q = b.Div(a);
                BigInteger y = a;
                a = b.Mod(a);
                b = y;
                y = d;
                d = x.Substract(q.Multiply(d));
                x = y;
            }
            x = x.Mod(n);
            if (x.CompareTo(Zero) == -1)//x<0
            {
                x = (x.Add(n)).Mod(n);
            }
            return x;
        }
        public bool ProbablyPrime()
        {
            return BigInteger.One.Equals(new BigInteger("2").Pow(this.Substract(One), this));
        }
        private static bool Witness(BigInteger a, BigInteger n)
        {
            BigInteger u = n.Substract(One);
            int t = 0;
            while (u.Mod(new BigInteger("2")).Equals(Zero))
            {
                t++;
                u = u.Div(new BigInteger("2"));
            }
            BigInteger[] x = new BigInteger[t + 1];
            x[0] = a.Pow(u, n);
            for (int i = 1; i <= t; i++)
            {
                x[i] = x[i - 1].Pow(new BigInteger("2"), n);
                if (x[i].Equals(One) && !x[i - 1].Equals(One) && !x[i - 1].Equals(n.Substract(One)))
                {
                    return true;
                }
            }
            if (!x[t].Equals(One))
            {
                return true;
            }
            return false;

        }
        public static BigInteger Generate(BigInteger n)
        {
            BigInteger maxBigInteger = n.Substract(One);
            BigInteger bigBase = new BigInteger(myBase.ToString());
            Random r = new Random();
            List<int> arr = new List<int>(maxBigInteger.arr.Count);

            bool flag = true;
            for (int i = maxBigInteger.arr.Count - 1; i >= 0; i--)
            {
                int temp;
                if (flag)
                {
                    temp = r.Next(maxBigInteger.arr[i] + 1);
                    flag = maxBigInteger.arr[i] == temp;
                }
                else
                {
                    temp = r.Next(myBase);
                }
                arr.Add(temp);
            }
            arr.Reverse();
            return new BigInteger(arr, true);
        }
        public static bool IsPrimeMillerRabin(BigInteger n, int s)
        {
            BigInteger pred = new BigInteger("0");
            for (int j = 0; j < s; j++)
            {
                BigInteger a = Generate(n);
                while (a.Equals(pred))
                {
                    a = Generate(n);
                }
                if (Witness(a, n))
                {
                    return false;
                }
            }
            return true;
        }
        public static BigInteger Generate(BigInteger a, BigInteger b)
        {
            return Generate(b.Substract(a)).Add(a);
        }
        public static BigInteger GeneratePrime(BigInteger a, BigInteger b)
        {
            int i = 0;
            BigInteger last = new BigInteger("1");
            while (i < 10000)
            {
                BigInteger temp = BigInteger.Generate(a, b);
                if (BigInteger.IsPrimeMillerRabin(temp, 100))
                {
                    return temp;
                }
                else
                {
                    last = temp;
                    i++;
                }
            }
            return null;//не нашли
        }
    }
}

