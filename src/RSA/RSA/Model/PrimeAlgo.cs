﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSA.Model
{
    class PrimeAlgo
    {
        private Random rand = new Random(DateTime.Now.Millisecond);
        private int[] primeArray;

        public int[] Sieve(int n)
        {
            var S = new int[n];
            S[1] = 0; // 1 - не простое число
            // заполняем решето единицами
            for (int k = 2; k <= n; k++)
                S[k] = 1;

            for (int k = 2; k*k <= n; k++)
            {
                // если k - простое (не вычеркнуто)
                if (S[k] == 1)
                {
                    // то вычеркнем кратные k
                    for (int l = k*k; l <= n; l += k)
                    {
                        S[l] = 0;
                    }
                }
            }

            /*S.Where(p => p != 0).ToArray();
            var res = new List<int>();
            for (int i = 2; i < n; i++)
            {
                if(S[i]!=0)
                    res.Add(i);
            }*/

            return S.Where(p => p != 0).ToArray();
        }

        public int GeneratePrimeNumber(int low, int high)
        { 
            var fitArray = Sieve(high).Where(p => p >= low).ToArray();
            return fitArray[rand.Next(fitArray.Length)];
        }

        public int InvMode(int x, int m)
        {
            return 0;
        }
        /*private int InvMod(int x, int m) 
        var x0 As Integer = x 
        var m0 As Integer = m 
        var t0 As Integer = 0 
        var t As Integer = 1 
        var q As Integer = m0 \ x0 
        var r As Integer = m0 - q * x0 
        While r > 0 
            var temp As Integer = t0 - q * t 
            If temp >= 0 Then temp = temp Mod m 
            If temp < 0 Then temp = m - ((-temp) Mod m) 
            t0 = t 
            t = temp 
            m0 = x0 
            x0 = r 
            q = m0 \ x0 
            r = m0 - q * x0 
        End While 
        var result As New Result 
        result.GCD = x0 
        If x0 <> 1 Then 
            result.IsDefined = False 
        Else 
            result.IsDefined = True 
            result.InvMod = t Mod m 
        End If 
        Return result 
    End Function 

        public int Inverse(int a, int n)
        {
            int b = n, x = 0, d = 1;
            while (a>0)//a>0
            {
                int q = b / a;
                int y = a;
                a = b % a;
                b = y;
                y = d;
                d = x - (q*d);
                x = y;
            }
            x = x.Mod(n);
            if (x.CompareTo(Zero) == -1)//x<0
            {
                x = (x.Add(n)).Mod(n);
            }
            return x;
        }*/
    }
}
